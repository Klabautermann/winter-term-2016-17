# -*- coding: utf-8 -*-
"""
Created on  

@author: fame
"""
 
import numpy as np 
from scipy.spatial import distance
from numpy import size as size

def compute_euclidean_distances( X, Y ) :
    """
    Compute the Euclidean distance between two matricess X and Y  
    Input:
    X: N-by-D numpy array 
    Y: M-by-D numpy array 
    
    Should return dist: M-by-N numpy array   
    """ 
    
    return np.transpose(distance.cdist(X,Y,'euclidean'));

    """
    An example of how we could compute the euclidean distance w/0 cdist:

    dist = np.zeros((size(X, 0), size(Y, 0)));

    for i in range(0, size(X,0)):
        for j in range(0, size(Y,0)):
            dist[i, j] = np.sqrt(np.sum((X[i,:] - Y[j,:]) ** 2));
    """       
 

def predict_labels( dists, labels, k=1):
    """
    Given a Euclidean distance matrix and associated training labels predict a label for each test point.
    Input:
    dists: M-by-N numpy array 
    labels: is a N dimensional numpy array
    
    Should return  pred_labels: M dimensional numpy array
    """
    return labels[np.argmin(dists, axis=1)];