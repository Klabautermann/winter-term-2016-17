function st = joint_st()
    tjrs=joint_tjrs;
    % use marginalization
    trs=squeeze(sum(tjrs,2));
    st=squeeze(sum(trs,2))';
end
